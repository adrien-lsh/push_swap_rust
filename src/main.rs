fn main() {
    let args = concatenate_args();
    let args = int_vec(&args);
    let args = get_indexes(&args);
    radix(&args);
}

fn int_vec(strings: &String) -> Vec<i32> {
    let mut v: Vec<i32> = Vec::new();
    for s in strings.split(' ') {
        if s.is_empty() {
            continue;
        }
        v.push(s.parse::<i32>().expect("sad"));
    }
    v
}
    
fn concatenate_args() -> String {
    let args: Vec<String> = std::env::args().collect();
    let mut concat = String::new();
    for arg in &args[1..(args.len())] {
        concat += &(arg.to_owned() + " ");
    }
    concat
}

fn get_indexes(v: &Vec<i32>) -> Vec<i32> {
    let mut sorted = v.to_vec();
    sorted.sort();
    let mut indexes: Vec<i32> = Vec::new();
    for e in v {
        for i in 0..(v.len()) {
            if sorted[i] == *e {
                indexes.push(i as i32);
                break;
            }
        }
    }
    indexes
}

fn radix(v: &Vec<i32>) {
    let mut step = 1;
    let mut a = v.to_vec();
    while !is_sorted(&a) {
        let mut b: Vec<i32> = Vec::new();
        let to_rotate = a.len();
        for _ in 0..to_rotate {
            if a[0] & step != 0 {
                b.push(a[0]);
                a.remove(0);
                println!("pb");
            } else {
                a.push(a[0]);
                a.remove(0);
                println!("ra");
            }
        }
        while !b.is_empty() {
            a.push(b[0]);
            b.remove(0);
            println!("pa");
        }
        step *= 2;
    }
}

fn is_sorted(v: &Vec<i32>) -> bool {
    for i in 0..(v.len() - 1) {
        if v[i] > v[i + 1] {
            return false;
        }
    }
    return true;
}